const getSum = (str1, str2) => {
  // add your implementation below
  if (typeof str1 !== 'string' || typeof str2 !== 'string'){
    return false
  }
  if (isNaN(str1) || isNaN(str2)){
    return false
  }
  if (str1 == ''){
    return str2
  }
  if (str2 == ''){
    return str1
  }
  let sum = parseInt(str1) + parseInt(str2)
  return sum.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let post = 0
  let comments = 0
  for (let obj of listOfPosts){
    if (obj.author === authorName){
      post++
    }
    if (obj.comments != null){
      for (let com of obj.comments){       
        if (com.author === authorName){
          comments++
        }
      }
    }
  }
  return `Post:${post},comments:${comments}`;
};

const tickets=(people)=> {
  // add your implementation below 
  let total = 0 //25
  
  for (let elem of people){
    if (total - parseInt(elem) < -25 ){
      return 'NO'
    }
    total += 25
  }

  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
